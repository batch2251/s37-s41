const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		if (result.length > 0) {

			let message = (result.length > 1) ? 'Duplicate User' : `email: ${reqBody.email}`

			return {
				message: message
			} 


		} else {

			return {
				message: "User not found"
			}
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if (error){
			return false;

		} else {
			return user
		}
	});
};

// User Authentication

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then( result => {

		if (result == null) {

			return {
				message: "Not found in our database"
			}

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}

			} else {

				return {
					message: "Password was incorrect"
				}
			};
		};
	});
};






// ====================================================================================
//           A C T I V I T Y 2
// ====================================================================================

// module.exports.userInfo = (reqBody) => {
// 	return User.findById(reqBody).then((result, err) => {
// 		if (err) {
// 			console.log(err);
// 			return false;
// 		} else {
// 			return result
// 		}
// 	})
// }

// copy from sir CJ

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};
// ==================================