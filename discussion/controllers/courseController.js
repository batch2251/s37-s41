const Course = require("../models/course");
const auth = require("../auth");

module.exports.addCourse = (req, res) => {
	return auth.verify(req, res, function(data) {
		if (data.isAdmin) {	
			return Course.find({name: req.body.name }).then( result => {		
				if (result.length > 0) {
					return { error: `Course Name: ${req.body.name} already exists in our database!` }
				} else {
					// start saving on mongodb
					let newCourse = new Course({
						name : req.body.name,
						description : req.body.description,
						price : req.body.price,
						isActive : req.body.isActive
					});

					return newCourse.save().then((course) => {
						return course
					}).catch( error => {
						return Promise.resolve(error)
					});
					// end saving on mongodb	
				}
			}).catch( error => {
				return Promise.resolve(error)
			});
		} else {
			return Promise.resolve({ error: 'ERROR :: For Admin Use Only!' })
		}	
	})
};

// A C T I V I T Y

module.exports.courseList = (req, res) => {

	return Course.find({}).then(result => {
		
		return result;
	})
}

