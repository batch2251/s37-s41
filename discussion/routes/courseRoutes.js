const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController")

// Route for Adding Course
router.post("/add-course", (req, res) => {
	courseController.addCourse(req, res).then( result => res.send(result) );
});

// A C T I V I T Y

router.get("/list", (req, res) => {
	courseController.courseList(req, res).then( result => res.send(result) );
});



module.exports = router;