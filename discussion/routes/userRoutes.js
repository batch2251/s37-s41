const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")

// Check Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

// Route for Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});

// Route for User Authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(result => res.send(result));
});

// ====================================================================================
//           A C T I V I T Y 2
// ====================================================================================

// router.post("/details", (req, res) => {
// 	userController.userInfo(req.body).then(result => res.send(result));
// })

// copy from sir CJ

// Route for retrieving user details
router.post("/details", (req, res) => {

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});
// ==================================

module.exports = router;