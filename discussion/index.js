// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const cors = require("cors");
 
const userRoute = require("./routes/userRoutes")
const courseRoute = require("./routes/courseRoutes")

const app = express();
const port = 7575;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Connect to MongoDB atlas
mongoose.set('strictQuery', true);
mongoose.connect(`mongodb+srv://${process.env.M_USERNAME}:${process.env.M_PASSWORD}@cluster0.z8dag7j.mongodb.net/course-booking-api?retryWrites=true&w=majority`,
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
  }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log("Connected to MongoDB!"));

app.use("/users", userRoute);
app.use("/course", courseRoute);


app.listen(port, () => console.log (`Now listening to port ${port}...`));